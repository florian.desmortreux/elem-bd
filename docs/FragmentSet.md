# Fragment Set

### Definition

A *Fragment Set* is a JSON object containing the [Fragments][4.1] and [Data Fragments][4.2]. 

### Type 

The project is not in Typescript, this is just for formal description

```ts
{
    [FragmentKey: string]: Fragment | FragmentKey,
    $: {
        [DataKey : string]: DataFragment
    }
}
```

### Properties

<dl><dt>

`FragmentKey` : string</dt><dd>

In a *Fragment Set* any property key is a *Fragment Key*. If a *Fragment Key* is the same as a [Template Element][2.1] id, then the value of this property will be merge in the [Template Element][2.1] when building. See [merging][4.1#merging].

> The value of a property can be another *Fragment Key*. In this case this is the value of this other *Fragment Key* that will be merge. This only apply once, which means that you shall not use a *Fragment Key* which value is a *Fragment Key* as a value. See [example](#wrong-fragment-key-use) 


</dd><dt>

`$`&emsp;Optional</dt><dd>

The `$` property is a special property of a *Fragment Set*. This is where the [Data Fragment][4.2] are stored in the set. Its value is a json object which key are *Data Keys* and values [Data Fragment][4.2].

> To see how to refer to [Data Fragment][4.2] in [Fragments][4.1], see [use of Data Fragment][4.1#use-data-fragment]
</dd>

</dl>

## Examples

### Fragment Set example

```js
import ElemBd from "./ElemBd.js";

// A ElemBd representing <p attr1="something" attr2="something"></p>
let template = [
    { id: "id1", tag: "p" },
    { id: "id2", tag: "p" },
    { id: "id3", tag: "p" },
    { id: "id4", tag: "p" }
]


// A Fragment Set
let fragmentSet = {
    id1: { textContent: "I'm blue" },         // A fragment
    id2: { textContent: "Dabadee Dabada" },   // A fragment
    id3: "id1",                                // Id of another fragment
    id4: "id2"                                 // Id of another fragment
}

// Build from template (ignore the controller)
let [element,] = ElemBd.build(template, fragmentSet);

document.body.append(element);
```

Result

```Html
<p>I'm blue</p>
<p>Dabadee Dabada</p>
<p>I'm blue</p>
<p>Dabadee Dabada</p>
```

### Wrong Fragment Key use

You can not use a *Fragment Key* as a value if the value associated to this key is an other *Fragment Key*. The value should be a [Fragment][4.1]

```js
import ElemBd from "./ElemBd.js";

// An incorrect Fragment Set
let fragmentSet = {
    blue1: { textContent: "I'm blue" },   // A fragment
    blue2: "blue1"                         // A correct Fragment Key
    blue3: "blue2", //  /!\ This is not correct since "blue2"
                    //  /!\ value is also a Fragment Key 
}
```

[ext.1]: https://developer.mozilla.org/fr/docs/Web/API/Element "Element"
[ext.2]: https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model "Document Object Model (DOM)"
[ext.3]: https://developer.mozilla.org/en-US/docs/Web/API/DocumentFragment "Document Fragment"

[1.0]: ./ElemBd.md "ElemBd"
[1.1]: ./ElemBd.build.md "ElemBd.build()"

[2.0]: ./Template.md "Template"
[2.1]: ./TemplateElement.md "Template Element"

[3.0]: ./Controller.md

[4.0]: ./FragmentSet.md "Fragment Set"
[4.1]: .Fragment.md "Fragment"
[4.1#use-data-fragment]: ./Fragment.md#use-data-fragment "Use Data Fragment"
[4.1#merging]: ./Fragment.md#merging "Merging"
[4.2]: ./DataFragment.md "Data Fragment"