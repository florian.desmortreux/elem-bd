## Data Fragment

A *Data Fragment* can be any valid json. All the *Data Fragment* are inside an object under the property named `$` of the [Fragment Set][4.0]. Each *Data fragment* has an id which is its key in the `$` object.

It allow you to refer to external data. You can define a [Fragment Set][4.0] with no `$` and fetch the required data before building the element.

### Warning

The *Data fragment* shall not be a [Fragment][4.1] or used as it. The behavior for this kind of usage is not define.

### Example

Fragment Set

```json
{
    "id1" : { 
        "attr" : { "title": "$element_title" },
        "textContent" : "$element_title"
    },
    "$": {
        "app_title" : "My Awesome element"
    }
}
```

Given the following [HtmlJsonElement](./Element.md)

```json
{ "id" : "id1", "tag" : "h1" }
```

**Result**

```Html
<h1 title="My Awesome element">My Awesome element</h1>
```

[ext.1]: https://developer.mozilla.org/fr/docs/Web/API/Element "Element"
[ext.2]: https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model "Document Object Model (DOM)"
[ext.3]: https://developer.mozilla.org/en-US/docs/Web/API/DocumentFragment "Document Fragment"

[1.0]: ./ElemBd.md "ElemBd"
[1.1]: ./ElemBd.build.md "ElemBd.build()"

[2.0]: ./Template.md "Template"
[2.1]: ./TemplateElement.md "Template Element"

[3.0]: ./Controller.md

[4.0]: ./FragmentSet.md "Fragment Set"
[4.1]: .Fragment.md "Fragment"
[4.1#use-data-fragment]: ./Fragment.md#use-data-fragment "Use Data Fragment"
[4.1#merging]: ./Fragment.md#merging "Merging"
[4.2]: ./DataFragment.md "Data Fragment"