# Template

A *Template* is a way to describe [Element][ext.1] of the [DOM][ext.2] with JSON.

It can be a single [Template Element][2.1] or a list of [Template Element][2.1]. If the *Template* is a list of [Template Element][2.1] the build function returns a [Document Fragment][ext.3].

## Example

### Controllable element

```js
import ElemBd from "./ElemBd.js";

// This is a Template composed of a single Template Element
let template = [
    { id: "@username", tag: "input", attr: { type: "text" }
    { id: "@sumbit",   tag: "button", textContent: "Log username" }
}

// Build from template 
let [element, controller] = ElemBd.build(template);

document.body.appendChild(element);

controller.submit.addEventListener('click', () => {
    console.log(controller.username.value)
})
```

Result

```Html
<input type="text">
<button>Log username</button>
```

[ext.1]: https://developer.mozilla.org/fr/docs/Web/API/Element "Element"
[ext.2]: https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model "Document Object Model (DOM)"
[ext.3]: https://developer.mozilla.org/en-US/docs/Web/API/DocumentFragment "Document Fragment"


[1.0]: ./ElemBd.md "ElemBd"
[1.1]: ./ElemBd.build.md "ElemBd.build()"

[2.0]: ./Template.md "Templates"
[2.1]: ./TemplateElement.md "Template Element"

[3.0]: ./Controller.md

[4.0]: ./FragmentSet.md "Fragment Set"
[4.1]: .Fragment.md "Fragment"
[4.1#use-data-fragment]: ./Fragment.md#use-data-fragment "Use Data Fragment"
[4.1#merging]: ./Fragment.md#merging "Merging"
[4.2]: ./DataFragment.md "Data Fragment"