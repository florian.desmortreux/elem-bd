# Controller

The *Controller* is a JS object from wich you can access contrallable elements. 

A controllable element in the [Template][2.0] is a [Template Element][2.1] with an id prefixed by a `@`. Then when building a element from ElemBd, you will get the element and its *Controller* allowing you to get access to elements of your template.

### Type

<!--- The project is not in Typescript, this is just for formal description -->

```ts
{
    [key: string] : Element
}
```

Keys are Template Elements ids (without `@`) and values the corresponding generated [Element][ext.1] after building.

> For this template
> ```json
> { 
>   id: "container", tag: "div", children: [
>       { id: "@paragraph", tag: "p",      textContent: "some text" },
>       { id: "@btn",       tag: "button", textContent: "click me"  }
>   ]
> }
> ```
> 
> You will get the following *Controller*
> 
> ```json
> {
>   paragraph: Element,
>   btn: Element
> }
> ```

### Example

#### Controllable element example

```js
import ElemBd from "./ElemBd.js";

let template = { 
    tag: "div", children: [
        { tag: "label", textContent: "I am not a controllabel element" },
        { id: "@input", tag: "input", attr: { placeholder: "I am a controllable element" } }
    ]
}

let [element, controller] = ElemBd.build(template);

document.body.appendChild(element);

console.log(controller.input.getAttribute("placeholder")) // Output "I am a controllable element"
```

**element** :

```html
<div>
    <label>I am not a controllabel element</label>
    <input placeholder="I am a controllable element">
</div>
```

[ext.1]: https://developer.mozilla.org/fr/docs/Web/API/Element "Element"
[ext.2]: https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model "Document Object Model (DOM)"
[ext.3]: https://developer.mozilla.org/en-US/docs/Web/API/DocumentFragment "Document Fragment"

[1.0]: ./ElemBd.md "ElemBd"
[1.1]: ./ElemBd.build.md "ElemBd.build()"

[2.0]: ./Template.md "Template"
[2.1]: ./TemplateElement.md "Template Element"

[3.0]: ./Controller.md

[4.0]: ./FragmentSet.md "Fragment Set"
[4.1]: .Fragment.md "Fragment"
[4.1#use-data-fragment]: ./Fragment.md#use-data-fragment "Use Data Fragment"
[4.1#merging]: ./Fragment.md#merging "Merging"
[4.2]: ./DataFragment.md "Data Fragment"