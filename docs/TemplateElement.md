# Template Element

A *Template Element* is a way to describe an [Element][ext.1] in json. 

Using [ElemBd.js][1.0] you will be able to create [Elements][ext.1] of the [DOM][ext.2] from a [Template][2.0]. A *Template Element* is the way to represent [Elements][ext.1] in a [Template][2.0]. 

If you want to get references to an element in the template after building it, you can make an element **controllable**. It means that it will save a reference in the [Controller][3.0] indexed by the id of the element. To make an element controllable prefix the id of this element with an `@`. The id of the element in the [Controller][3.0] does not contains the `@`. 

### Properties

<dl style="padding-left: 2em;"><dt>

`id`</dt><dd>
    
The id of the *Template Element*. 
> To make an element controllable prefix this id with `@` 

> Does not affect the id attribute of the generated [Element][ext.1]. For that see the `attr` property below</dd><dt>

`tag`&emsp;Required</dt><dd>
    
Tag of the [Element][ext.1] it describe</dd><dt>

`attr`</dt><dd>
    
Attributes of the [Element][ext.1] in the form of key value pair</br>
> Setting an attibute to `true` will toggle it on. If that is not what you want, set the value to `"true"`. See [example](#boolean-attribute-example)</dd><dt>

`textContent`</dt><dd>
    
The text content of the [Element][ext.1]</dd><dt>
    
`children`</dt><dd>
    
Children of the [Element][ext.1] as *Template Elements*. See [example](#div-with-children)</dd>
</dl>


## Examples

### Single Paragraph

```js
import ElemBd from "./ElemBd.js";

// This is a Template composed of a single Template Element
let template = { 
    id: "element_id", 
    tag: "p",
    attr: { title: "I'm a tooltip" },
    textContent: "Hello world"
}

// Build from template ignoring the controller
let [element,] = ElemBd.build(template);

document.body.appendChild(element);
```

Result

```Html
<p title="I'm a tooltip">Hello world</p>
```

### Boolean attribute example

```js
import ElemBd from "./ElemBd.js";

// This is a Template composed of a single Template Element
let template1 = { tag: "input", attr: { type: "text", disabled: true, }};
let template2 = { tag: "input", attr: { type: "text", disabled: "true", }};

// Build from template ignoring the controller
let [element1,] = ElemBd.build(template1);
let [element2,] = ElemBd.build(template2);

document.body.appendChild(element1);
document.body.appendChild(element2);
```

Result

```Html
<input type="text" disabled>            <!-- element1 -->
<input type="text" disabled="true">     <!-- element2 -->
```

### Div with children

```js
import ElemBd from "./ElemBd.js";

// This is a Template composed of a single Template Element
let template = { tag: "div", children: [
    { tag: "h1", textContent: "I am a title" },
    { tag: "p",  textContent: "Hello world" }
]};

// Build from template ignoring the controller
let [element,] = ElemBd.build(template);

document.body.appendChild(element);
```

Result

```Html
<div>
    <h1>I am a title</h1>
    <p>Hello world</p>
</div>
```

[ext.1]: https://developer.mozilla.org/fr/docs/Web/API/Element "Element"
[ext.2]: https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model "Document Object Model (DOM)"
[ext.3]: https://developer.mozilla.org/en-US/docs/Web/API/DocumentFragment "Document Fragment"

[1.0]: ./ElemBd.md "ElemBd"
[1.1]: ./ElemBd.build.md "ElemBd.build()"

[2.0]: ./Template.md "Template"
[2.1]: ./TemplateElement.md "Template Element"

[3.0]: ./Controller.md

[4.0]: ./FragmentSet.md "Fragment Set"
[4.1]: .Fragment.md "Fragment"
[4.1#use-data-fragment]: ./Fragment.md#use-data-fragment "Use Data Fragment"
[4.1#merging]: ./Fragment.md#merging "Merging"
[4.2]: ./DataFragment.md "Data Fragment"