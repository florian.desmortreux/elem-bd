# ElemBd

The exported value of the ElemBd.js module

### Properties

It has no properties

### Methods

<dl><dt>

[`ElemBd.build()`](./ElemBd.build.md)
</dt><dd>

Create an [Element][ext.1] and its [Controller][3.0].

</dd></dl>


[ext.1]: https://developer.mozilla.org/fr/docs/Web/API/Element "Element"
[ext.2]: https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model "Document Object Model (DOM)"
[ext.3]: https://developer.mozilla.org/en-US/docs/Web/API/DocumentFragment "Document Fragment"

[1.0]: ./ElemBd.md "ElemBd"
[1.1]: ./ElemBd.build.md "ElemBd.build()"

[2.0]: ./Template.md "Template"
[2.1]: ./TemplateElement.md "Template Element"

[3.0]: ./Controller.md

[4.0]: ./FragmentSet.md "Fragment Set"
[4.1]: .Fragment.md "Fragment"
[4.1#use-data-fragment]: ./Fragment.md#use-data-fragment "Use Data Fragment"
[4.1#merging]: ./Fragment.md#merging "Merging"
[4.2]: ./DataFragment.md "Data Fragment"