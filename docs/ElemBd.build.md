# ElemBd.build()

Create an [Element][ext.1] and its [Controller][3.0] from a [Template][2.0] and optionnals [Fragment Sets][4.0].

If the [Template][2.0] is a list of [Template Element][2.1] returns a [Document Fragment][ext.3].

### Syntax

```js
build(template)
build(template, fragmentSets)
```

### Parameters

<dl><dt>

`template`</dt><dd>

The [Template][2.0] of the element to build.

> A [Template][2.0] is defined as single [TemplateElement][2.1] or a list of [Template Element][2.1] </dd><dt>

`fragmentSets`&emsp;Optionnal</dt><dd>

This parameter can be a [Fragment Set][4.0] or a list of [Fragment Set][4.0]
> In the case you provide a list of [Fragment Set][4.0], they will be applied in the order of the list. It means that the last will override the values of the others when merging properties. See [Merging][4.1#merging]

</dd></dl>

### Return

It return an array containing the element generated and the controller

```js
// Get both
let [element, controller] = ElemBd.build(template);

// Get only element
let [element, ] = ElemBd.buid(template);
```

[ext.1]: https://developer.mozilla.org/fr/docs/Web/API/Element "Element"
[ext.2]: https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model "Document Object Model (DOM)"
[ext.3]: https://developer.mozilla.org/en-US/docs/Web/API/DocumentFragment "Document Fragment"

[1.0]: ./ElemBd.md "ElemBd"
[1.1]: ./ElemBd.build.md "ElemBd.build()"

[2.0]: ./Template.md "Template"
[2.1]: ./TemplateElement.md "Template Element"

[3.0]: ./Controller.md

[4.0]: ./FragmentSet.md "Fragment Set"
[4.1]: .Fragment.md "Fragment"
[4.1#use-data-fragment]: ./Fragment.md#use-data-fragment "Use Data Fragment"
[4.1#merging]: ./Fragment.md#merging "Merging"
[4.2]: ./DataFragment.md "Data Fragment"