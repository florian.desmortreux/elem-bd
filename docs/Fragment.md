# Fragment

The *Fragment* is used to describe properties that you can give to a [Template Element][2.1]. In a *Fragment* you can also refer to [Data Fragments][4.2] ( See [how](#data-fragment) ).

A *Fragment* is merge in a [Template Element][2.1] when its key in the fragment set is the same as the id of the [Template Element][2.1]. See [Merging](#merging).


See [Template Element][2.1], [Fragment Set][4.0], [Data Fragments][4.2].

### Properties

Same as [Template Element][2.1] but you shall not use `id` or `tag` in a *Fragment*.

For now use of `children` in a *Fragment* is not intended. The behavior for such property is not define. 


## Examples

### Single Fragment

```js
import ElemBd from "./ElemBd.js";

// Let's use the template of a <p> element
let template = { 
    id: "myId", 
    tag: "p", 
    attr: { attr1: "something", attr2: "something" }
}

// A Fragment Set with a single Fragment
let fragmentSet = {
    myId : { 
        attr : { title: "I'm a tooltip", attr2: "else" },
        textContent: "Hello world"
    }
}

// Build from template and fragmentSet ignoring the controller
let [element,] = ElemBd.build(template, fragmentSet);

document.body.append(element);
```

Result

```Html
<p attr1="something" attr2="else" title="I'm a tooltip">Hello world</p>
```

### Use Data Fragment

To use a [Data Fragment][4.2] in a *Fragment*, set the value of one property in the *Fragment* to a *Data Key* prefixed by a `$`.

```js
import ElemBd from "./ElemBd.js";

// Let's use the template of a <p> element
let template = { id: "myId", tag: "p" }

// A Fragment Set with a single Fragment
let fragmentSet = {
    myId: { 
        attr: { title: "$title" },
        textContent: "$text"
    },
    $: {
        title: "I'm a tooltip",
        text:  "Hello World"
    }
}

// Build from template and fragmentSet ignoring the controller
let [element,] = ElemBd.build(template, fragmentSet);

document.body.append(element);
```
Result

```Html
<p title="I'm a tooltip">Hello world</p>
```

# Merging

The merging is the operation between a [Template Element][2.1] and a [Fragment][4.1]. It consist in adding the properties of the [Fragment][4.1] in the [Template Element][2.1].

When merging, the properties of the [Fragment][4.1] will override properties whith the same keys recursively in the [Template Element][2.1].

## Examples

### Depth one merging

As expected, depth one properties such as strings are overridden

```js
let template_element = { id: "one", tag: "p", textContent: "default" };

let template_fragment = { textContent: "new text" };

// ...
// After merging the two

console.log(template_element);

{ id: "one", tag : "p", textContent: "new text" }
```

Result

```html
<p>new text</p>
```

## Depth two merging

Here you can see that merging does not override the attr property totally but instead go down one layer to merge the properties.

```js
let template_element = { 
    id: "one",
    tag: "input", 
    attr: {
        type: "text",
        required: true
    }
};

let template_fragment = { 
    attr: {
        required: false,
        disabled: true
    }
};

// ...
// After merging the two

console.log(template_element);
{ 
    id: "one",
    tag: "input",
    attr: {
        required: false,
        disabled: true 
    }
}
```

Result

```html
<input disabled>
```

[ext.1]: https://developer.mozilla.org/fr/docs/Web/API/Element "Element"
[ext.2]: https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model "Document Object Model (DOM)"
[ext.3]: https://developer.mozilla.org/en-US/docs/Web/API/DocumentFragment "Document Fragment"

[1.0]: ./ElemBd.md "ElemBd"
[1.1]: ./ElemBd.build.md "ElemBd.build()"

[2.0]: ./Template.md "Template"
[2.1]: ./TemplateElement.md "Template Element"

[3.0]: ./Controller.md

[4.0]: ./FragmentSet.md "Fragment Set"
[4.1]: .Fragment.md "Fragment"
[4.1#use-data-fragment]: ./Fragment.md#use-data-fragment "Use Data Fragment"
[4.1#merging]: ./Fragment.md#merging "Merging"
[4.2]: ./DataFragment.md "Data Fragment"