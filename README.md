# ElemBd

## Introduction

ElemBd is a front-end Javascript module to build Html DOM elements from templates. For now template are written in JSON. ElemBd template give two additional feature to building elements.

First, it allow to get an javascript object with references to the elements built from template. This feature avoid parsing HTML and then call `getElementById()` to get the reference to an element. The generated JS object is call *[controller][3.0]*.

 Second, you can use a complementary template system called *[FragmentSet][4.0]* to add/change properties of elements of a template. This allows to define variants of a template. For exemple, a multilingual element can have a default template in english and a *[FragmentSet][4.0]* for each other language supported.


## Getting started

To create an [Element][ext.1] you need to write a *[Template][2.0]*. A *[Template][2.0]* is either a [Template Element][2.1] or an array of [Template Element][2.1].

Then import **ElemBd.js** module and call its `build` methods. It will return the generated Element and its controller. In the following example, no controllable element have been defined so the controller will be an empty object.

```js
import ElemBd from "./ElemBd.js";

let template = { tag : "p", textContent: "Hello World" };

let [element, controller] = ElemBd.build(template);

document.body.appendChild(element);
console.log(controller); // Outputs : {}
```

If you want to ignore the controller, you can use the following syntax :

```js
let [element, ] = ElemBd.build(template);
```

## Doc

⚠ Documentation may be incomplete, still a work in progress

1. [ElemBd][1.0]
   1. [ElemBd.build()][1.1]
2. [Template][2.0]
   1. [Template Element][2.1]
3. [Controller][3.0]
4. [Fragment Set][4.0]
   1. [Fragment][4.1]
   2. [Data Fragment][4.2]


[ext.1]: https://developer.mozilla.org/fr/docs/Web/API/Element "Element"
[ext.2]: https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model "Document Object Model (DOM)"
[ext.3]: https://developer.mozilla.org/en-US/docs/Web/API/DocumentFragment "Document Fragment"

[1.0]: ./docs/ElemBd.md "ElemBd"
[1.1]: ./docs/ElemBd.build.md "ElemBd.build()"

[2.0]: ./docs/Template.md "Template"
[2.1]: ./docs/TemplateElement.md "Template Element"

[3.0]: ./docs/Controller.md

[4.0]: ./docs/FragmentSet.md "Fragment Set"
[4.1]: .Fragment.md "Fragment"
[4.1#use-data-fragment]: ./docs/Fragment.md#use-data-fragment "Use Data Fragment"
[4.1#merging]: ./docs/Fragment.md#merging "Merging"
[4.2]: ./docs/DataFragment.md "Data Fragment"